#!/bin/sh

set -e

case $ANDROID_ABI in
armeabi*)
	target=android-arm
	;;
arm64-v8a)
	target=android-arm64
	;;
x86)
	target=android-x86
	;;
x86_64)
	target=android-x86_64
	;;
*)
	target=android
	;;
esac

toolchain_path=$(dirname $ANDROID_CXX_COMPILER)
export PATH="$toolchain_path:$PATH"

perl $SOURCE_DIR/Configure shared $target

make -j$(nproc) SHLIB_EXT=.so

cp libcrypto.so $OUTPUT_DIR/libcrypto.so
cp libssl.so $OUTPUT_DIR/libssl.so

