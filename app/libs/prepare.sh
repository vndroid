#!/bin/sh

set -ve

tar xvf openssl-1.1.1.tar.gz
ln -sf openssl-1.1.1 openssl

tar xvf libjpeg-turbo-2.0.0.tar.gz
ln -sf libjpeg-turbo-2.0.0 libjpeg-turbo

tar xvf LibVNCServer-0.9.11.tar.gz
ln -sf libvncserver-LibVNCServer-0.9.11 libvncserver
(cd libvncserver && patch -p1 < ../libvncserver-LibVNCServer-0.9.11.patch)
