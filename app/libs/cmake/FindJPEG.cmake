
set(JPEG_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/libs/libjpeg-turbo ${CMAKE_BINARY_DIR}/libjpeg-turbo)
set(JPEG_LIBRARIES jpeg)

find_package_handle_standard_args(JPEG "Could NOT find JPEG"
    JPEG_LIBRARIES
    JPEG_INCLUDE_DIR
  )

