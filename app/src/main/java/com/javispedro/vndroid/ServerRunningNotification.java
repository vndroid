package com.javispedro.vndroid;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;

import androidx.core.app.NotificationCompat;

/**
 * Helper class for showing and canceling server running
 * notifications.
 * <p>
 * This class makes heavy use of the {@link NotificationCompat.Builder} helper
 * class to create notifications in a backward-compatible way.
 */
public class ServerRunningNotification {
    /**
     * The unique identifier for this type of notification.
     */
    private static final String NOTIFICATION_TAG = "ServerRunning";

    private static final String NOTIFICATION_CHANNEL = NOTIFICATION_TAG;

    public static NotificationChannel createNotificationChannel(final Context context) {
        final Resources res = context.getResources();
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL,
                res.getString(R.string.server_running_notification_channel),
                NotificationManager.IMPORTANCE_NONE);
        channel.setShowBadge(false);
        return channel;
    }

    public static void initNotificationChannel(final Context context) {
        NotificationManager manager = context.getSystemService(NotificationManager.class);
        manager.createNotificationChannel(createNotificationChannel(context));
    }

    public static Notification build(final Context context) {
        final Resources res = context.getResources();

        final String title = res.getString(R.string.server_running_notification_title);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL)

                // Set appropriate defaults for the notification light, sound,
                // and vibration.
                .setDefaults(Notification.DEFAULT_ALL)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setOngoing(true)

                // Set required fields, including the small icon, the
                // notification title, and text.
                .setSmallIcon(R.drawable.ic_stat_server_running)
                .setContentTitle(title)
                //.setContentText(text)

                // All fields below this line are optional.

                // Use a default priority (recognized on devices running Android
                // 4.1 or later)
                .setPriority(NotificationCompat.PRIORITY_MIN)

                // Set ticker text (preview) information for this notification.
                //.setTicker(ticker)

                // If this notification relates to a past or upcoming event, you
                // should set the relevant time information using the setWhen
                // method below. If this call is omitted, the notification's
                // timestamp will by set to the time at which it was shown.
                // TODO: Call setWhen if this notification relates to a past or
                // upcoming event. The sole argument to this method should be
                // the notification timestamp in milliseconds.
                //.setWhen(...)

                // Set the pending intent to be initiated when the user touches
                // the notification.
                .setContentIntent(
                        PendingIntent.getActivity(
                                context,
                                0,
                                new Intent(context, SettingsActivity.class),
                                PendingIntent.FLAG_UPDATE_CURRENT))

                // Example additional actions for this notification. These will
                // only show on devices running Android 4.1 or later, so you
                // should ensure that the activity in this notification's
                // content intent provides access to the same actions in
                // another way.
                .addAction(
                        R.drawable.ic_action_stop_screen_sharing,
                        res.getString(R.string.action_stop),
                        PendingIntent.getForegroundService(
                                context,
                                0,
                                new Intent(context, ServerService.class).setAction(ServerService.ACTION_STOP_SERVER),
                                PendingIntent.FLAG_UPDATE_CURRENT));

        return builder.build();
    }
}
