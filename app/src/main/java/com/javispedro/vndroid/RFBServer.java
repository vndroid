package com.javispedro.vndroid;

import android.graphics.PixelFormat;
import android.media.Image;

import androidx.annotation.Nullable;

import java.nio.ByteBuffer;

public class RFBServer {
    private static final String TAG = RFBServer.class.getSimpleName();

    @Nullable
    private Image lastImage = null;

    @Nullable
    private EventCallback callback = null;

    public class ServerException extends RuntimeException {
        public ServerException(String what) {
            super(what);
        }
    }

    public interface EventCallback {
        void onPointerEvent(int buttonMask, int x, int y);
        void onKeyEvent(int key, boolean state);
        void onClientEvent();
    }

    public RFBServer() {
        if (!allocate()) {
            throw new ServerException("failure to allocate()");
        }
    }

    public void start() {
        if (!init()) {
            throw new ServerException("failure to init()");
        }
    }

    public void stop() {
        shutdown();
        forgetLastImage();
    }

    public void finalize() {
        shutdown();
        forgetLastImage();
        deallocate();
    }

    public void setEventCallback(EventCallback c) {
        callback = c;
        set_event_callback(c);
    }

    public void putImage(Image image) {
        Image.Plane[] planes = image.getPlanes();

        if (image.getFormat() != PixelFormat.RGBA_8888) {
            String msg = "Unknown pixel format: " + image.getFormat();
            throw new UnsupportedOperationException(msg);
        }
        if (planes.length != 1) {
            String msg = "Unknown number of planes: " + planes.length;
            throw new UnsupportedOperationException(msg);
        }

        Image.Plane plane = planes[0];

        if (!put_image(image.getWidth(), image.getHeight(), plane.getBuffer(), plane.getPixelStride(), plane.getRowStride())) {
            throw new ServerException("failed to put image");
        }

        forgetLastImage();
        lastImage = image;
    }

    public int getNumClients() {
        return get_num_clients();
    }

    private void forgetLastImage() {
        if (lastImage != null) {
            lastImage.close();
        }
        lastImage = null;
    }

    private long nativeData = 0;

    private native boolean allocate();
    private native void deallocate();

    private native boolean init();
    private native void shutdown();

    private native void set_event_callback(EventCallback c);

    private native boolean put_image(int width, int height, ByteBuffer buffer, int pixel_stride, int row_stride);

    private native int get_num_clients();
}
