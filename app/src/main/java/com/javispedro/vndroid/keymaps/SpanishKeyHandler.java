package com.javispedro.vndroid.keymaps;

import java.text.Normalizer;

import static com.javispedro.vndroid.keymaps.KeySyms.Key_dead_acute;
import static com.javispedro.vndroid.keymaps.KeySyms.Key_dead_grave;

public class SpanishKeyHandler extends KeyHandler {

    private char deadKey = 0;

    @Override
    public boolean keyDown(int key) {
        if (key >= KeySyms.Key_space && key <= KeySyms.Key_z) {
            // Printable keysyms
            char c = (char) (' ' + (key - KeySyms.Key_space));
            if (deadKey != 0) {
                StringBuilder builder = new StringBuilder(2);
                builder.append(c);
                builder.append(deadKey);
                String text = Normalizer.normalize(builder.toString(), Normalizer.Form.NFKC);
                action.postText(text);
                deadKey = 0;
                return true;
            }
            action.postChar(c);
            return true;
        } else {
            switch (key) {
                case KeySyms.Key_ccedilla:
                    action.postChar('ç');
                    return true;
                case KeySyms.Key_ntilde:
                    action.postChar('ñ');
                    return true;
                case Key_dead_acute:
                    deadKey = '\u0301';
                    return true;
                case Key_dead_grave:
                    deadKey = '\u0300';
                    return true;
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int key) {
        return false;
    }
}
