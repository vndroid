package com.javispedro.vndroid.keymaps;

public interface KeyActionHandler {
    int ACTION_NONE = 0;
    int ACTION_BACKSPACE = 1;
    int ACTION_DEL = 2;
    int ACTION_MOVE_LEFT = 10;
    int ACTION_MOVE_RIGHT = 11;
    int ACTION_MOVE_LEFTMOST = 12;
    int ACTION_MOVE_RIGHTMOST = 13;
    int ACTION_SELECT_LEFT = 20;
    int ACTION_SELECT_RIGHT = 21;
    int ACTION_SELECT_LEFTMOST = 22;
    int ACTION_SELECT_RIGHTMOST = 23;

    void postText(CharSequence text);
    void postChar(char c);
    void postLocalAction(int action);
    void postGlobalAction(int action);
}
