package com.javispedro.vndroid.keymaps;

public abstract class KeyHandler {
    protected KeyActionHandler action;

    public void setActionHandler(KeyActionHandler handler) {
        action = handler;
    }

    public abstract boolean keyDown(int key);
    public abstract boolean keyUp(int key);
}
