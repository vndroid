package com.javispedro.vndroid.keymaps;

import android.accessibilityservice.AccessibilityService;

public class AndroidKeyHandler extends KeyHandler {
    private boolean shiftPressed = false;

    @Override
    public boolean keyDown(int key) {
        switch (key) {
            case KeySyms.Key_Shift_L:
            case KeySyms.Key_Shift_R:
                shiftPressed = true;
                return false;
            case KeySyms.Key_Escape:
                action.postGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                return true;
            case KeySyms.Key_F1:
                action.postGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                return true;
            case KeySyms.Key_BackSpace:
                action.postLocalAction(KeyActionHandler.ACTION_BACKSPACE);
                return true;
            case KeySyms.Key_Delete:
                action.postLocalAction(KeyActionHandler.ACTION_DEL);
                return true;
            case KeySyms.Key_Left:
                if (shiftPressed) {
                    action.postLocalAction(KeyActionHandler.ACTION_SELECT_LEFT);
                } else {
                    action.postLocalAction(KeyActionHandler.ACTION_MOVE_LEFT);
                }
                return true;
            case KeySyms.Key_Right:
                if (shiftPressed) {
                    action.postLocalAction(KeyActionHandler.ACTION_SELECT_RIGHT);
                } else {
                    action.postLocalAction(KeyActionHandler.ACTION_MOVE_RIGHT);
                }
                return true;
            case KeySyms.Key_Home:
                if (shiftPressed) {
                    action.postLocalAction(KeyActionHandler.ACTION_SELECT_LEFTMOST);
                } else {
                    action.postLocalAction(KeyActionHandler.ACTION_MOVE_LEFTMOST);
                }
                return true;
            case KeySyms.Key_End:
                if (shiftPressed) {
                    action.postLocalAction(KeyActionHandler.ACTION_SELECT_RIGHTMOST);
                } else {
                    action.postLocalAction(KeyActionHandler.ACTION_MOVE_RIGHTMOST);
                }
                return true;
        }
        return false;
    }

    @Override
    public boolean keyUp(int key) {
        switch (key) {
            case KeySyms.Key_Shift_L:
            case KeySyms.Key_Shift_R:
                shiftPressed = false;
                return false;
        }
        return false;
    }
}
