package com.javispedro.vndroid;

import android.content.Context;
import android.content.ContextWrapper;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.util.Log;

import androidx.annotation.Nullable;

public abstract class ScreenGrabber extends ContextWrapper {
    private static final String TAG = ScreenGrabber.class.getSimpleName();

    protected static final int MAX_IMAGES = 3;

    @Nullable protected ImageReader reader;
    @Nullable protected VirtualDisplay display;
    @Nullable protected Callback callback;

    public interface Callback {
        void onImage(Image image);
    }

    protected class VirtualDisplayCallback extends VirtualDisplay.Callback {
        @Override
        public void onPaused() {
            Log.d(TAG, "onVirtualDisplayPaused");
        }

        @Override
        public void onResumed() {
            Log.d(TAG, "onVirtualDisplayResumed");
        }

        @Override
        public void onStopped() {
            Log.d(TAG, "onVirtualDisplayStopped");
        }
    }

    protected class ImageReaderCallback implements ImageReader.OnImageAvailableListener {
        @Override
        public void onImageAvailable(ImageReader reader) {
            if (callback != null) {
                Image image = reader.acquireLatestImage();
                callback.onImage(image);
            }
        }
    }

    protected ScreenGrabber(Context context) {
        super(context);
    }

    public abstract void start();

    public void stop() {
        if (display != null) {
            display.release();
            display = null;
        }
        if (reader != null) {
            reader.close();
            reader = null;
        }
    }

    public void setCallback(Callback c) {
        callback = c;
    }

    public int scaleInputX(int x) {
        return x;
    }

    public int scaleInputY(int y) {
        return y;
    }

    public boolean hasSizeChanged() { return false; }

    public void updateScreenSize() { }
}
