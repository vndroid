package com.javispedro.vndroid;

import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.display.DisplayManager;
import android.media.ImageReader;
import android.util.Log;

public class ScreenVirtualGrabber extends ScreenGrabber {
    private static final String TAG = ScreenVirtualGrabber.class.getSimpleName();

    public ScreenVirtualGrabber(Context context) {
        super(context);
    }

    @Override
    public void start() {
        if (reader != null || display != null) {
            Log.w(TAG, "already started");
            return;
        }

        int width = 1024;
        int height = 768;
        int dpi = 96;

        Log.d(TAG, "starting virtual display with width " + width + " height: " + height + " dpi: " + dpi);

        reader = ImageReader.newInstance(width, height, PixelFormat.RGBA_8888, MAX_IMAGES);
        reader.setOnImageAvailableListener(new ImageReaderCallback(), null);

        DisplayManager manager = getSystemService(DisplayManager.class);
        display = manager.createVirtualDisplay("vndroid",
                width, height, dpi, reader.getSurface(),
                DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC | DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY,
                new VirtualDisplayCallback(), null);
    }
}
