package com.javispedro.vndroid;

import android.accessibilityservice.AccessibilityService;
import android.content.Intent;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import androidx.annotation.Nullable;

public class ControlService extends AccessibilityService {
    private final String TAG = ControlService.class.getSimpleName();

    @Nullable
    private static ControlService instance = null;

    @Override
    public void onServiceConnected() {
        super.onServiceConnected();
        Log.d(TAG, "onServiceConnected");
        instance = this;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        instance = null;
        return super.onUnbind(intent);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

    }

    @Override
    public void onInterrupt() {

    }

    @Nullable
    public static ControlService getInstance() {
        return instance;
    }

    public static boolean isServiceStarted() {
        return instance != null;
    }
}
